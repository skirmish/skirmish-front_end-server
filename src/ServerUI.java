import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class ServerUI {
	private final JFrame frame;
	private final JButton close;
	private String serverAddress;
	public ServerUI(String serverAddress){
		this.serverAddress = serverAddress;
		frame = new JFrame("SKIRMISH | Front End Server");
		close = new JButton("CLOSE");
		init();
	}
	
	private void init(){
		close.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
			}
		});
		setupCanvas();
		
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
	}

	public void display(){
		frame.setVisible(true);
	}
	
	private void setupCanvas() {
		JPanel canvas = new JPanel();
		canvas.setPreferredSize(new Dimension(300,50));
		JLabel text = new JLabel("Filtering for "+serverAddress);
		canvas.add(text);
		canvas.add(close);
		
		frame.setContentPane(canvas);
	}
}
