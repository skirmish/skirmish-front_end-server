import javax.swing.JOptionPane;

import exceptions.InvalidPortException;



public class Main {
	public static void main(String[] args) {
		int ownport = askPort("Own");
		if(0==ownport)return;
		
		String serveraddress = Utils.askAddress();
		if(serveraddress == null)System.exit(0);
		
		int serverport = askPort("Server");
		if(0==ownport)return;
		
		if(ownport == serverport){
			Utils.popupError("Cannot be the same ports! Please run again...");
			return;
		}
		
		FrontEndServer server = new FrontEndServer(ownport, serveraddress, serverport);
		
		server.display();
	}
	
	private static int askPort(String s){
		int port = 0;
		while(port == 0){
			String portStr = JOptionPane.showInputDialog(null, s+" Port: ", "SKIRMISH | FRONT END SERVER", JOptionPane.QUESTION_MESSAGE);
			if(portStr == null){
				return 0;
			}else{
				try{
					port = Validate.port(portStr);
				}catch(InvalidPortException ipe){
					Utils.popupError(ipe.getMessage());
					port = 0;
				}
			}
		}
		return port;
	}
}
