import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import exceptions.InvalidServerAddressException;
import exceptions.PacketReceiveException;
import exceptions.PortUnavailableException;

public class FrontEndServer {
	/* SOCKET PROGRAMMING ATTRIBUTES */
	private static final int MAXIMUM_BUFFER_SIZE = 1024;
	private final DatagramSocket serverSocket;
	private volatile boolean alive;
	private Thread clientListener;
	private InetAddress serverAddress;
	private int serverPort;
	
	/* GUI ATTRIBUTES*/
	private ServerUI ui;
	public FrontEndServer(int ownport, String serveraddress, int serverport) {
		try {
			serverSocket = new DatagramSocket(ownport);
			this.serverAddress = InetAddress.getByName(serveraddress);
			this.serverPort = serverport;
			setupClientListener();
			startClientListener();
			ui = new ServerUI(serveraddress+":"+serverport);
			ui.display();
		} catch (SocketException e) {
			throw new PortUnavailableException("Cannot listen to port "+ownport+".");
		} catch (UnknownHostException e) {
			throw new InvalidServerAddressException("Unable to find server: "+serverAddress);
		}
	}

	private void startClientListener() {
		clientListener.start();
	}

	private void setupClientListener() {
		clientListener = new Thread(new Runnable(){
			@Override
			public void run() {
				alive = true;
				while(alive){
					byte[] buff = new byte[MAXIMUM_BUFFER_SIZE];
					DatagramPacket packet = new DatagramPacket(buff,buff.length);
					
					try {
						serverSocket.receive(packet);
						String data = new String(buff).trim();
						
						//check if data is valid keyword 
						if(data.startsWith("CONNECTION_REQUEST") || 
								data.startsWith("CONNECTION_REQUEST") ||
								data.startsWith("CONNECTED") ||
								data.startsWith("CHAT") ||
								data.startsWith("ATTACK") ||
								data.startsWith("TROOP")){
							//source
							//InetAddress sourceAddress = packet.getAddress();
							//int sourcePort = packet.getPort();
							
							//forward to backend server
							packet.setAddress(serverAddress);
							packet.setPort(serverPort);
							
							serverSocket.send(packet);
						}
						
						
						} catch (IOException e) {
						System.err.println("ERROR.");
						throw new PacketReceiveException("Server socket cannot receive packets!");
					}
				}
				
				if(serverSocket != null){
					serverSocket.close();
				}
			}
		});
		
	}

	public void display() {
		ui.display();
	}

}
